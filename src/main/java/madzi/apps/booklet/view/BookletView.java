package madzi.apps.booklet.view;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import madzi.apps.booklet.cdi.StartupScene;

@ApplicationScoped
public class BookletView {

    private static final String RESOURCE_PATH = "/view/booklet.fxml";

    @Inject
    private FXMLLoader fxmlLoader;

    public void start(@Observes @StartupScene Stage stage) throws IOException {
        try (final InputStream fxmlStream = getClass().getResourceAsStream(RESOURCE_PATH)) {
            Parent root = fxmlLoader.load(fxmlStream);
            stage.setScene(new Scene(root));

            stage.show();
        }
    }
}
