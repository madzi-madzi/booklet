package madzi.apps.booklet;

import jakarta.enterprise.inject.se.SeContainer;
import jakarta.enterprise.inject.se.SeContainerInitializer;
import jakarta.enterprise.util.AnnotationLiteral;
import javafx.application.Application;
import javafx.stage.Stage;
import madzi.apps.booklet.cdi.StartupScene;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App extends Application {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    private SeContainer container;

    @Override
    public void init() {
        logger.info("Starting application");
        final var containerInitializer = SeContainerInitializer.newInstance();
        container = containerInitializer.initialize();
    }

    @Override
    public void stop() {
        logger.info("Stopping application");
        container.close();
    }

    @Override
    public void start(Stage stage) throws Exception {
        container.getBeanManager().fireEvent(stage, new AnnotationLiteral<StartupScene>() {});
    }

    public static void main( String[] args ) {
        launch(args);
    }
}
