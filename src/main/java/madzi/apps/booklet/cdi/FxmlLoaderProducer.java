package madzi.apps.booklet.cdi;

import jakarta.enterprise.inject.Instance;
import jakarta.enterprise.inject.Produces;
import jakarta.inject.Inject;
import javafx.fxml.FXMLLoader;

public class FxmlLoaderProducer {

    @Inject
    private Instance<Object> instance;

    @Produces
    public FXMLLoader createLoader() {
        final var loader = new FXMLLoader();
        loader.setControllerFactory(param -> instance.select(param).get());

        return loader;
    }
}
