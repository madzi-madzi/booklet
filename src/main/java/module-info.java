module madzi.apps.booklet {
    requires org.slf4j;
    requires org.slf4j.simple;
    requires javafx.controlsEmpty;
    requires javafx.controls;
    requires javafx.graphicsEmpty;
    requires javafx.graphics;
    requires javafx.baseEmpty;
    requires javafx.base;
    requires javafx.fxmlEmpty;
    requires javafx.fxml;
    requires jakarta.inject;
    requires jakarta.cdi;

    opens madzi.apps.booklet to weld.core.impl;
    opens madzi.apps.booklet.cdi to weld.core.impl;
    opens madzi.apps.booklet.view to weld.core.impl;

    exports madzi.apps.booklet;
}
